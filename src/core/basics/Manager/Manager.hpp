#ifndef MANAGER_HPP
#define MANAGER_HPP

#include "../../basics/Object/Object.hpp"

namespace kp
{
	class Manager : public kp::Object
	{
	private:
	protected:
	public:
		Manager(kp::Object* parent);
		~Manager();
	};
}

#endif // !MANAGER_HPP
