#ifndef COMPONENT_HPP
#define COMPONENT_HPP

#include "../../basics/Object/Object.hpp"

namespace kp
{
	class Component : public kp::Object
	{
	private:
	protected:
	public:
	};
}

#endif // !COMPONENT_HPP
