#ifndef OBJECT_HPP
#define OBJECT_HPP

#include <cstdint>
#include <string>

namespace kp
{
    class Object
    {
    protected:
        kp::Object* m_parent;
        std::string m_objectName;
    public:
        explicit Object(kp::Object* parent = nullptr);
        virtual ~Object();

        std::string& getObjectName();

        virtual void update(float dt);
    };
}

#endif // !OBJECT_HPP
