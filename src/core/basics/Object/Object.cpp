#include "Object.hpp"

kp::Object::Object(kp::Object* parent)
    : m_parent(parent), m_objectName("Object")
{
}

kp::Object::~Object()
{
    delete m_parent;
}

std::string& kp::Object::getObjectName()
{
    return m_objectName;
}

void kp::Object::update(float dt)
{
}
