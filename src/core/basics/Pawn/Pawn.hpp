#ifndef PAWN_HPP
#define PAWN_HPP

#include "../../basics/Object/Object.hpp"

namespace kp
{
	class Pawn : public kp::Object
	{
	private:
	protected:
	public:
		explicit Pawn(kp::Object* parent = nullptr);
		virtual ~Pawn();
	};
}

#endif // !PAWN_HPP