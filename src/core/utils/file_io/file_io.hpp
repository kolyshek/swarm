#ifndef FILE_IO_HPP
#define FILE_IO_HPP

#include <string>
#include <fstream>
#include <iostream>

namespace kp
{
	std::string readFromFile(std::string& file);
	void writeToFile(std::string& file, std::string& data);
}

#endif // !FILE_IO_HPP
