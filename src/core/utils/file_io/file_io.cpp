#include "file_io.hpp"

std::string kp::readFromFile(std::string& file)
{
	std::string data;
	std::ifstream ifs;
	ifs.open(file, std::ios::in);
	if (ifs.is_open())
	{
		ifs >> data;
	}
	ifs.close();
	return data;
}

void kp::writeToFile(std::string& file, std::string& data)
{
	std::ofstream ofs;
	ofs.open(file, std::ios::out);
	if (ofs.is_open())
	{
		ofs << data;
	}
	ofs.close();
}
