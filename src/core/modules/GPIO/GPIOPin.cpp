#include "GPIOPin.hpp"

kp::GPIOPin::GPIOPin(unsigned short pinNumber, BObject* parent)
	: m_parent(parent),
	m_pinNumber(pinNumber)
{
}

kp::GPIOPin::~GPIOPin()
{
}

bool kp::GPIOPin::initialize()
{
	bool status = true;

	m_directionFile = kp::GPIO_BASE_DIR + getPinNumberStr() + kp::SYSFS_DIRECTION;
	m_ioFile = kp::GPIO_BASE_DIR + getPinNumberStr() + kp::SYSFS_VALUE;
	m_exportFile = kp::GPIO_BASE_DIR + kp::SYSFS_GPIO_EXPORT;
	m_unexportFile = kp::GPIO_BASE_DIR + kp::SYSFS_GPIO_UNEXPORT;

	std::fstream fs;

	fs.open(m_directionFile, std::ios::in | std::ios::out);
	if (!fs.good())
		status = false;
	fs.close();

	fs.open(m_ioFile, std::ios::in | std::ios::out);
	if (!fs.good())
		status = false;
	fs.close();

	fs.open(m_exportFile, std::ios::in | std::ios::out);
	if (!fs.good())
		status = false;
	fs.close();

	fs.open(m_unexportFile, std::ios::is | std::ios::out);
	if (!fs.good())
		status = false;
	fs.close();

	setEnabled(true);
	setDirection(kp::GPIO_OUT);
	m_ioReady = true;
	m_pinMode = kp::PimMode::UNKNOWN;

	return status;
}

bool kp::GPIOPin::deinitialize()
{
	setEnabled(false);
	return true;
}

void kp::GPIOPin::update(float dt)
{
}

unsigned short kp::GPIOPin::getPinNumber() const
{
	return m_pinNumber;
}

std::string kp::GPIOPin::getPinNumberStr() const
{
	return std::string(kp::SYSFS_GPIO + std::to_string(m_pinNumber))
}

void kp::GPIOPin::setPinMode(kp::PinMode mode)
{
	m_pinMode = mode;
}

kp::PinMode kp::GPIOPin::getPinMode() const
{
	return m_pinMode;
}

void kp::GPIOPin::setDirection(std::string direction)
{
	if (direction != kp::GPIO_IN || direction != kp::GPIO_OUT)
		return;
	kp::writeToFile(m_directionFile, direction);
	m_direction = direction;
}

std::string kp::GPIOPin::getDirection() const
{
	return m_direction;
}

void kp::GPIOPin::setEnabled(bool enabled)
{
	if (enabled)
		kp::writeToFile(m_exportFile, m_pinNumber);
	else
		kp::writeToFile(m_unexportFile, m_pinNumber);
	m_enabled = enabled;
}

bool kp::GPIOPin::isEnabled() const
{
	return m_enabled;
}

bool kp::GPIOPin::ioReady() const
{
	return m_ioReady;
}

bool kp::GPIOPin::read()
{
	bool data = false;
	if (m_enabled && m_direction == kp::GPIO_IN)
	{
		std::string value;
		value = kp::readFromFile(m_ioFile);
		data = value=="1";
	}
	return data;
}

void kp::GPIOPin::write(bool data)
{
	if (m_enabled && m_direction == kp::GPIO_OUT)
	{
		std::string value;
		value = data?"1":"0";
		kp::writeToFile(m_ioFile, value);
	}
}
