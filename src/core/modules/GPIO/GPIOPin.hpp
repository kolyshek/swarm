#ifndef GPIO_PIN_HPP
#define GPIO_PIN_HPP

#include <fstream>
#include <atomic>

#include "../../../core/basics/Object/Object.hpp"
#include "../../utils/file_io/file_io.hpp"
#include "gpio_defs.hpp"

namespace kp
{
	class GPIOPin : public kp::Object
	{
	private:
		unsigned short m_pinNumber;
		std::string m_directionFile;
		std::string m_ioFile;
		std::string m_exportFile;
		std::string m_unexportFile;

	protected:
		kp::PinMode m_pinMode;
		std::atomic_bool m_enabled;
		std::atomic_bool m_ioReady;
		std::string m_direction;

	public:
		explicit GPIOPin(unsigned short pinNumber, Object* parent = nullptr);
		virtual ~GPIOPin();

		bool initialize();
		bool deinitialize();
		virtual void update(float dt) override;

		unsigned short getPinNumber() const;
		std::string getPinNumberStr() const;

		void setPinMode(kp::PinMode mode);
		kp::PinMode getPinMode() const;

		void setDirection(std::string direction);
		std::string getDirection() const;

		void setEnabled(bool enabled);
		bool isEnabled() const;

		bool ioReady() const;

		bool read();
		void write(bool data);
	};
}

#endif // !GPIO_PIN_HPP
