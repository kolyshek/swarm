#ifndef GPIO_DEFS_HPP
#define GPIO_DEFS_HPP

#include <string>

namespace kp {
	const std::string SYSFS_GPIO_LIST     = "/sys/kernel/debug/gpio";
	const std::string GPIO_BASE_DIR       = "/sys/class/gpio";
	const std::string SYSFS_GPIO_EXPORT   = "/export";
	const std::string SYSFS_GPIO_UNEXPORT = "/unexport";
	const std::string SYSFS_GPIO          = "/gpio";
	const std::string SYSFS_DIRECTION     = "/direction";
	const std::string SYSFS_VALUE         = "/value";
	const std::string GPIO_IN             = "in";
	const std::string GPIO_OUT            = "out";

	struct GPIOPinInfo
	{
		uint16_t physical_number;
		uint16_t sysfs_number;
		bool enabled;
		std::string status;
		std::string direction;
		bool value;
	};

	enum class PinMode
	{
		UNKNOWN,
		DIGITAL,
		ANALOG,
		MAX_MODES
	};
}

#endif // !GPIO_DEFS_HPP
