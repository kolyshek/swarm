#ifndef GPIO_HANDLER_HPP
#define GPIO_HANDLER_HPP

#include <vector>

#include "../../basics/Object.hpp"
#include "GPIOPin.hpp"

namespace kp
{
	class GPIOHandler : public Object
	{
	private:
	protected:
		std::vector<kp::GPIOPin*> m_pins;
	public:
		explicit GPIOHandler();
		virtual ~GPIOHandler();

		bool initialize();
		bool deinitialize();
	}
}

#endif // !GPIO_HANLDER_HPP
