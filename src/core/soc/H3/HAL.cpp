#include "HAL.hpp"

const std::string kp::HAL::M_SOC_NAME = "AllWinnerH3";

kp::HAL::HAL(kp::Object* parent)
	: kp::Object(parent)
{
}

kp::HAL::~HAL()
{
}

std::string kp::HAL::getSoCName()
{
	return M_SOC_NAME;
}
