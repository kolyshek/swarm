#ifndef H3_HAL_HPP
#define H3_HAL_HPP

#include <string>

#include "../../../core/basics/Object/Object.hpp"

namespace kp
{
    class HAL : public kp::Object
	{
	private:
		static const std::string M_SOC_NAME;
	public:
		explicit HAL(kp::Object* parent = nullptr);
		virtual ~HAL();

		static std::string getSoCName();
	};
}

#endif // !H3_HAL_HPP
