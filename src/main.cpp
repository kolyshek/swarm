#ifdef MASTER_ACTOR_TYPE
#include "actors/master/AMaster.hpp"
#endif

#ifdef SLAVE_ACTOR_TYPE
#include "actors/slave/ASlave.hpp"
#endif

int32_t main()
{
#ifdef MASTER_ACTOR_TYPE
	kp::AMaster master;
	return master.run();
#endif

#ifdef SLAVE_ACTOR_TYPE
	kp::ASlave slave;
	return slave.run();
#endif

    return 0;
}
