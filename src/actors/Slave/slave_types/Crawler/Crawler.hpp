#ifndef CRAWLER_HPP
#define CRAWLER_HPP

#include "../../../../core/basics/Pawn/Pawn.hpp"

namespace kp
{
    class Crawler : public kp::Pawn
    {
    private:
    protected:
    public:
        explicit Crawler(kp::Object* parent = nullptr);
        virtual ~Crawler();
    };
}

#endif // !CRAWLER_HPP