#ifndef HEXAPOD_HPP
#define HEXAPOD_HPP

#include "../../../../core/basics/Pawn/Pawn.hpp"

namespace kp
{
    class Hexapod : public kp::Pawn
    {
    private:
    protected:
    public:
        explicit Hexapod(kp::Object* parent = nullptr);
        virtual ~Hexapod();
    };
}

#endif // !HEXAPOD_HPP