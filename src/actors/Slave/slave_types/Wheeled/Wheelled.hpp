#ifndef WHEELLED_HPP
#define WHEELLED_HPP

#include "../../../../core/basics/Pawn/Pawn.hpp"

namespace kp
{
    class Wheelled : public kp::Pawn
    {
    private:
    protected:
    public:
        explicit Wheelled(kp::Object* parent = nullptr);
        virtual ~Wheelled();
    };    
}

#endif // !WHEELLED_HPP