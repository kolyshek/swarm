#ifndef PROPELLER_HPP
#define PROPELLER_HPP

#include "../../../../core/basics/Pawn/Pawn.hpp"

namespace kp
{
	class Propeller : public kp::Pawn
	{
	private:
	protected:
	public:
		explicit Propeller(kp::Object* parent = nullptr);
		virtual ~Propeller();
	};
}

#endif // !PROPELLER_HPP
