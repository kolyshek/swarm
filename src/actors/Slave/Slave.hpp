#ifndef SLAVE_HPP
#define SLAVE_HPP

#include "../../core/basics/Pawn/Pawn.hpp"

namespace kp
{
	class Slave
		: public kp::Pawn
	{
	private:
	protected:
	public:
		explicit Slave(kp::Object* parent = nullptr);
		virtual ~Slave();
	};
}

#endif // !SLAVE_HPP
