#ifndef MASTER_HPP
#define MASTER_HPP

#include "../../core/basics/Manager/Manager.hpp"

namespace kp
{
	class Master : public kp::Manager
	{
	private:
	protected:
	public:
		explicit Master(kp::Object* parent = nullptr);
		virtual ~Master();
	};
}

#endif // !MASTER_HPP
