#ifndef COORDINATOR_HPP
#define COORDINATOR_HPP

#include "../../core/basics/Manager/Manager.hpp"

namespace kp
{
	class Coordinator : public kp::Manager
	{
	private:
	protected:
	public:
		explicit Coordinator(kp::Object* parent = nullptr);
		virtual ~Coordinator();
	};
}

#endif // !COORDINATOR_HPP
