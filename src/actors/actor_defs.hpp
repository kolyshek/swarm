#ifndef ACTOR_DEFS_HPP
#define ACTOR_DEFS_HPP

namespace kp
{
	enum class SlaveType
	{
		UNKNOWN,
		CRAWLER,
		PROPELLER,
		WHEELED,
		MAX_TYPES
	}; 
}

#endif // !ACTOR_DEFS_HPP
