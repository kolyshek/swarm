CC = g++

CFLAGS = -c -Wall
LDFLAGS =

SOURCEDIR = src
SOURCES = $(wildcard $(SOURCEDIR)/*.cpp)
BUILDDIR = build
OBJECTS = $(SOURCES:.cpp=.o)

EXECUTABLE = swarm

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@
